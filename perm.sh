#!/bin/sh
echo ""
echo "._______________________ Permission Changer Laravel (v.5.5.*) ____________________."
echo "|                                                                                 |"
echo "| ./perm.sh project/folder/ owner/username                                        |"
echo "|                                                                                 |"
echo "|                                                 create with <3 by Gandhi Wibowo |"
echo "!_________________________________________________________________________________!"
echo ""

if [ $# -eq 0 ]
  then
    echo ""
    echo "._______________________ Running Permission Changer  ........ ____________________."
    echo "|                                                                                 |"
    echo "| Error : Please input the foldername of your project                             |"
    echo "!_________________________________________________________________________________!"
    echo ""
  else
    if [ $# -eq 1 ]
	then
	  sudo chown -R $2:www-data $1
	else
	  sudo chown -R gandhi:www-data $1
    fi
    echo ""
    echo "._______________________ Running Permission Changer  ........ ____________________."
    echo "|                                                                                 |"
    echo "| Changing Permission for $1 project                                              |"
    echo "!_________________________________________________________________________________!"
    echo ""

    sudo find $1 -type f -exec chmod 664 {} \;
    sudo find $1 -type d -exec chmod 775 {} \;
    cd $1 && sudo chgrp -R www-data storage bootstrap/cache && sudo chmod -R ug+rwx storage bootstrap/cache
    echo ""
    echo ".________________________ Finish Permission Changer  ........ ____________________."
    echo "|                                                                                 |"
    echo "| Done !                                                                          |"
    echo "!_________________________________________________________________________________!"
    echo ""

fi

